/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.fit.dpo.dpo_project_mav.graphics;


import javax.swing.JFrame;
import javax.swing.JPanel;

/**
 *
 * @author Radek
 */
public class ViewFrame extends JFrame{

    private JPanel activePanel = null;
    
    public ViewFrame() {
        super("Project Manager");
        
        setLocation(350, 100);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setVisible(true);
        pack();
    }
    
    public void showPanel(JPanel panel){
        if(activePanel != null){
            activePanel.setVisible(false);
            remove(activePanel);
        }
        activePanel = panel;
        add(panel);
        panel.setVisible(true);
        pack();
        invalidate();
    }

}


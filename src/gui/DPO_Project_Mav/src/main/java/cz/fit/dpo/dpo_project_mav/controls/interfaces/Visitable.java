/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.fit.dpo.dpo_project_mav.controls.interfaces;

import cz.fit.dpo.dpo_project_mav.controls.visitors.PanelDataParserVisitor;
import cz.fit.dpo.dpo_project_mav.graphics.visitors.PanelNewDialogVisitor;

/**
 *
 * @author Radek
 */
public interface Visitable {
    public void accept(PanelNewDialogVisitor v);
}

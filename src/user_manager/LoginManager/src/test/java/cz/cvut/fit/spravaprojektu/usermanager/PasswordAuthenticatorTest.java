package cz.cvut.fit.spravaprojektu.usermanager;


import cz.cvut.fit.spravaprojektu.usermanager.exception.AuthenticationException;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import static org.mockito.Mockito.*;


/**
 *
 * @author bestajin@fit.cvut.cz
 */
public class PasswordAuthenticatorTest {

    IPasswordRepository passwordRepository;

    PasswordAuthenticator authenticator;

    IUser user;

    AuthToken token;


    @Before
    public void init() {
        user = mock(IUser.class);
        token = mock(AuthToken.class);
        passwordRepository = mock(IPasswordRepository.class);
        authenticator = new PasswordAuthenticator(passwordRepository);
    }


    @Test
    public void shouldPassUsernameToPasswordRepository() {
        when(passwordRepository.find("homer")).thenReturn("beer");
        when(user.getUserName()).thenReturn("homer");
        authenticator.authenticate(user, token);
        verify(passwordRepository, times(1)).find("homer");
    }


    @Test
    public void shouldUsePasswordRepository() throws AuthenticationException {
        when(passwordRepository.find(anyString())).thenReturn(null);
        authenticator.authenticate(user, token);
        verify(passwordRepository, times(1)).find(user.getUserName());
    }


    @Test
    public void shouldUseTokenCredentialsForAuthentication() throws AuthenticationException {
        when(passwordRepository.find(anyString())).thenReturn("password");
        authenticator.authenticate(user, token);
        verify(token, times(1)).getCredentials();
    }


    @Test
    public void shouldFailIfPasswordNotFound() throws AuthenticationException {
        when(passwordRepository.find(anyString())).thenReturn(null);
        when(token.getCredentials()).thenReturn("");
        assertFalse(authenticator.authenticate(user, token));
    }


    @Test
    public void shouldFailIfPasswordIsIncorrect() {
        when(passwordRepository.find(anyString())).thenReturn("topSecret");
        when(token.getCredentials()).thenReturn("johny alba");
        assertFalse(authenticator.authenticate(user, token));
    }


    @Test
    public void shouldSucceedIfPasswordIsCorrect() {
        when(passwordRepository.find(anyString())).thenReturn("secret");
        when(token.getCredentials()).thenReturn("secret");
        assertTrue(authenticator.authenticate(user, token));
    }
}

package cz.cvut.fit.spravaprojektu.usermanager;


import java.util.ArrayList;
import java.util.List;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import static org.mockito.Mockito.*;


/**
 *
 * @author bestajin@fit.cvut.cz
 */
public class RoleManagerTest {

    private IRolesRepository roleRepository;

    private RoleManager roleManager;

    private IUser user;


    @Before
    public void init() {
        user = mock(IUser.class);
        roleRepository = mock(IRolesRepository.class);
        roleManager = new RoleManager(roleRepository);
    }


    @Test
    public void shouldUseRoleRepository() {
        when(roleRepository.findRoles(anyString())).thenReturn(new ArrayList<String>());
        roleManager.hasRole(user, "administrator");
        verify(roleRepository, times(1)).findRoles(user.getUserName());
    }


    @Test
    public void shouldTestRolesAgainstRoleRepository() {
        List<String> roles = new ArrayList<String>();
        roles.add("administrator");
        when(roleRepository.findRoles(anyString())).thenReturn(roles);
        assertTrue(roleManager.hasRole(user, "administrator"));
    }


    @Test
    public void shouldUseUsersUsername() {
        when(roleRepository.findRoles(anyString())).thenReturn(new ArrayList<String>());
        roleManager.hasRole(user, "role");
        verify(user, times(1)).getUserName();
    }


    @Test
    public void shouldReturnFalseIfNoRolesAreFound() {
        when(roleRepository.findRoles(anyString())).thenReturn(null);
        assertFalse(roleManager.hasRole(user, "role"));
    }
}

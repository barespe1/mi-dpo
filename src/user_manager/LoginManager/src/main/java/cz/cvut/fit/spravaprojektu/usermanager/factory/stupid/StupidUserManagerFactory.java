/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package cz.cvut.fit.spravaprojektu.usermanager.factory.stupid;

import cz.cvut.fit.spravaprojektu.usermanager.AuthToken;
import cz.cvut.fit.spravaprojektu.usermanager.IAuthenticator;
import cz.cvut.fit.spravaprojektu.usermanager.IUser;
import cz.cvut.fit.spravaprojektu.usermanager.UserManager;

/**
 *
 * @author jilly
 */
public class StupidUserManagerFactory {
    
    
    /**
     * Vyrobi velice ale velice stupidni implementaci UserManageru ;-)
     * 
     * @return 
     */
    public UserManager createUserManager() {
        UserManager manager = new UserManager(new StupidUserRepository(), new IAuthenticator() {
            public boolean authenticate(IUser user, AuthToken authToken) {
                return user.getUserName().equals("pavel") && "fit".equals(authToken.getCredentials());
            }
        });
        
        return manager;
    }
}

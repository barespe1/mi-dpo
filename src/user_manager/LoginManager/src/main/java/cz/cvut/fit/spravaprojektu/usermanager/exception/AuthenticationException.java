package cz.cvut.fit.spravaprojektu.usermanager.exception;


/**
 *
 * @author bestajin@fit.cvut.cz
 */
public class AuthenticationException extends Exception {

    public AuthenticationException() {
    }


    public AuthenticationException(String string) {
        super(string);
    }


    public AuthenticationException(String string, Throwable thrwbl) {
        super(string, thrwbl);
    }


    public AuthenticationException(Throwable thrwbl) {
        super(thrwbl);
    }


    public AuthenticationException(String string, Throwable thrwbl, boolean bln, boolean bln1) {
        super(string, thrwbl, bln, bln1);
    }
}

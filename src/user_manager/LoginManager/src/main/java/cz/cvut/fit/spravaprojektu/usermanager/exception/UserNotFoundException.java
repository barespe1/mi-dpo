package cz.cvut.fit.spravaprojektu.usermanager.exception;


/**
 *
 * @author bestajin@fit.cvut.cz
 */
public class UserNotFoundException extends AuthenticationException {

    public UserNotFoundException() {
    }


    public UserNotFoundException(String string) {
        super(string);
    }


    public UserNotFoundException(String string, Throwable thrwbl) {
        super(string, thrwbl);
    }


    public UserNotFoundException(Throwable thrwbl) {
        super(thrwbl);
    }


    public UserNotFoundException(String string, Throwable thrwbl, boolean bln, boolean bln1) {
        super(string, thrwbl, bln, bln1);
    }
}

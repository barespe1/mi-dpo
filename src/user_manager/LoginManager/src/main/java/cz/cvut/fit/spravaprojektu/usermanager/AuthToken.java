package cz.cvut.fit.spravaprojektu.usermanager;


/**
 * Rozhraní pro různé typy přihlašování
 *
 * @author bestajin@fit.cvut.cz
 */
public interface AuthToken {

    /**
     * Vrátí objekt reprezentující přihlašovací údaj. Např. v případě přihlášení
     * pomocí hesla vrátí heslo.
     *
     * @return přihlašovací údaj pro kontrolu autentizace
     */
    public Object getCredentials();
}

package cz.cvut.fit.spravaprojektu.usermanager;


/**
 * Repository pro pristup k uzivatelskym uctum
 *
 * @author bestajin@fit.cvut.cz
 */
public interface IUserRepository {

    /**
     * Vrátí uživatele identifikovaného podle uživatelského jména, pokud takový
     * neexistuje, vrací
     * <code>NULL</code>.
     *
     * @param username
     * @return
     */
    IUser findUser(String username);
}

package cz.cvut.fit.spravaprojektu.usermanager;


import cz.cvut.fit.spravaprojektu.usermanager.exception.AuthenticationException;
import cz.cvut.fit.spravaprojektu.usermanager.exception.NotInRoleException;
import java.util.List;


/**
 *
 * @author bestajin@fit.cvut.cz
 */
public class RoleManager implements IRoleManager {

    private final IRolesRepository rolesRepository;


    public RoleManager(IRolesRepository rolesRepository) {
        this.rolesRepository = rolesRepository;
    }


    public boolean hasRole(IUser user, String roleIdentifier) {
        List<String> roles = rolesRepository.findRoles(user.getUserName());

        if (roles == null) {
            return false;
        }

        if (roles.contains(roleIdentifier)) {
            return true;
        }

        return false;
    }
}

package cz.cvut.fit.spravaprojektu.usermanager.exception;


/**
 *
 * @author bestajin@fit.cvut.cz
 */
public class NoPermissionException extends AuthenticationException {

    public NoPermissionException() {
    }


    public NoPermissionException(String string) {
        super(string);
    }


    public NoPermissionException(String string, Throwable thrwbl) {
        super(string, thrwbl);
    }


    public NoPermissionException(Throwable thrwbl) {
        super(thrwbl);
    }


    public NoPermissionException(String string, Throwable thrwbl, boolean bln, boolean bln1) {
        super(string, thrwbl, bln, bln1);
    }
}

package cz.cvut.fit.spravaprojektu.usermanager.exception;


/**
 *
 * @author bestajin@fit.cvut.cz
 */
public class NotInRoleException extends AuthenticationException {

    public NotInRoleException() {
    }


    public NotInRoleException(String string) {
        super(string);
    }


    public NotInRoleException(String string, Throwable thrwbl) {
        super(string, thrwbl);
    }


    public NotInRoleException(Throwable thrwbl) {
        super(thrwbl);
    }


    public NotInRoleException(String string, Throwable thrwbl, boolean bln, boolean bln1) {
        super(string, thrwbl, bln, bln1);
    }
}

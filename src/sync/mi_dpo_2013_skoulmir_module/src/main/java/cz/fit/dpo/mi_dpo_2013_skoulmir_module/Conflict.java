package cz.fit.dpo.mi_dpo_2013_skoulmir_module;

import cz.fit.dpo.mi_dpo_2013_skoulmir_dummy.ITask;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author skoulmir
 */
public class Conflict {
    private ITask master;
    private ITask local;
    
    public Conflict(ITask master, ITask local){
        this.master = master;
        this.local = local;
    }
    
    public ITask getMaster(){
        return master;
    }
    public ITask getLocal(){
        return local;
    }
}

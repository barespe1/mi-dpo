/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.fit.dpo.mi_dpo_2013_skoulmir_dummy;

import cz.cvut.fit.spravaprojektu.usermanager.UserModel;
import java.util.Collection;

/**
 *
 * @author mini
 */
public class Project extends ITask{
    private Collection<ITask> projects;

    public Project(Collection<ITask> projects, String name, String description, UserModel owner, State state) {
        super(name, description, owner, state);
        this.projects = projects;
    }
    
    public Collection<ITask> getProjects() {
        return projects;
    }

    public void setProjects(Collection<ITask> projects) {
        this.projects = projects;
        this.edited = true;
    }
    
    @Override
    public void addTo(DBConnector db){
        db.addProject(this);
    }
    @Override
    public void removeFrom(DBConnector db){
        db.removeProject(this);
    }
    @Override
    public int compareTo(ITask project) {
        if(project instanceof Project){
            return this.name.compareTo(((Project)project).name);
        }
        throw new UnsupportedOperationException("Not supported.");
    }
    
    @Override
    public void acceptVisitor(ITaskVisitor taskVisitor) {
        taskVisitor.visit(this);
    }
}

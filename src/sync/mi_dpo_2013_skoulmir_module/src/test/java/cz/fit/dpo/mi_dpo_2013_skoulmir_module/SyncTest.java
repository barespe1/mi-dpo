/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.fit.dpo.mi_dpo_2013_skoulmir_module;

import cz.fit.dpo.mi_dpo_2013_skoulmir_dummy.DBConnector;
import cz.fit.dpo.mi_dpo_2013_skoulmir_dummy.ITask;
import cz.fit.dpo.mi_dpo_2013_skoulmir_dummy.Project;
import cz.fit.dpo.mi_dpo_2013_skoulmir_dummy.Task;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.fail;
import junit.framework.TestCase;

/**
 *
 * @author skoulmir
 */
public class SyncTest extends TestCase {
    
    public SyncTest(String testName) {
        super(testName);
    }

    /**
     * Test of getConflicts method, of class Sync.
     */
    public void testGetConflicts() {
        System.out.println("getConflicts");
        Sync instance = new Sync();
        Task taskLocal = new Task(new Date(), new Date(), 25, "task", "desc", null, null);
        Task taskMaster = new Task(new Date(), new Date(), 25, "task", "desc", null, null);
        DBConnector dbMaster = new DBConnector();
        DBConnector dbLocal = new DBConnector();
        dbLocal.addtask(taskLocal);
        dbMaster.addtask(taskMaster);
        instance.setLocal(dbLocal);
        instance.setMaster(dbMaster);
        instance.synchronize();
        List expResult = new ArrayList();
        expResult.add(new Conflict(taskMaster, taskLocal));
        List result = instance.getConflicts();
        for (int i = 0; i < expResult.size(); i++) {
            assertEquals(((Conflict)expResult.get(i)).getLocal(), 
                    ((Conflict)result.get(i)).getLocal());
            assertEquals(((Conflict)expResult.get(i)).getMaster(), 
                    ((Conflict)result.get(i)).getMaster());
        }
    }

    /**
     * Test of resolve method, of class Sync.
     */
    public void testResolve() {
        System.out.println("resolve");
        boolean override = false;
        Task taskLocal = new Task(new Date(), new Date(), 25, "task", "desc", null, null);
        Task taskMaster = new Task(new Date(), new Date(), 25, "task", "desc", null, null);
        DBConnector dbMaster = new DBConnector();
        DBConnector dbLocal = new DBConnector();
        dbLocal.addtask(taskLocal);
        dbMaster.addtask(taskMaster);
        Conflict conflict = new Conflict(taskMaster, taskLocal);
        Sync instance = new Sync();
        instance.setLocal(dbLocal);
        instance.setMaster(dbMaster);
        instance.resolve(override, conflict);
        assertEquals(taskMaster, dbMaster.getTasks(null).get(0));
        assertEquals(taskMaster, dbLocal.getTasks(null).get(0));
    }
    
    /**
     * Test of resolve method, of class Sync.
     */
    public void testResolveOverride() {
        System.out.println("resolve");
        boolean override = true;
        Task taskLocal = new Task(new Date(), new Date(), 25, "task", "desc", null, null);
        Task taskMaster = new Task(new Date(), new Date(), 25, "task", "desc", null, null);
        DBConnector dbMaster = new DBConnector();
        DBConnector dbLocal = new DBConnector();
        dbLocal.addtask(taskLocal);
        dbMaster.addtask(taskMaster);
        Conflict conflict = new Conflict(taskMaster, taskLocal);
        Sync instance = new Sync();
        instance.setLocal(dbLocal);
        instance.setMaster(dbMaster);
        instance.resolve(override, conflict);
        assertEquals(taskLocal, dbMaster.getTasks(null).get(0));
        assertEquals(taskLocal, dbLocal.getTasks(null).get(0));
    }
    

    /**
     * Test of setLocal method, of class Sync.
     */
    public void testSetLocal() {
        System.out.println("setLocal");
        DBConnector local = new DBConnector();
        Sync instance = new Sync();
        instance.setLocal(local);
        assertEquals(local, local);
    }

    /**
     * Test of setMaster method, of class Sync.
     */
    public void testSetMaster() {
        System.out.println("setMaster");
        DBConnector master = new DBConnector();
        Sync instance = new Sync();
        instance.setMaster(master);
        assertEquals(master, master);
    }

    /**
     * Test of synchronize method, of class Sync.
     */
    public void testSynchronize() {
        System.out.println("synchronize");
        System.out.println("getConflicts");
        Sync instance = new Sync();
        Task taskLocal = new Task(new Date(), new Date(), 25, "task1", "desc", null, null);
        Task taskMaster = new Task(new Date(), new Date(), 25, "task2", "desc", null, null);
        Project projectLocal = new Project(new LinkedList<ITask>(), "project1", "desc", null, null);
        Project projectMaster = new Project(new LinkedList<ITask>(), "project2", "desc", null, null);
        DBConnector dbMaster = new DBConnector();
        DBConnector dbLocal = new DBConnector();
        dbLocal.addtask(taskLocal);
        dbMaster.addProject(projectLocal);
        dbMaster.addtask(taskMaster);
        dbMaster.addProject(projectMaster);
        instance.setLocal(dbLocal);
        instance.setMaster(dbMaster);
        instance.synchronize();
        boolean expResult = true;
        boolean result = instance.synchronize();
        assertEquals(expResult, result);
        for (int i = 0; i < 2; i++) {
            ITask t1 = dbLocal.getTasks(null).get(i);
            ITask t2 = dbMaster.getTasks(null).get(i);
            ITask p1 = dbLocal.getProject(null).get(i);
            ITask p2 = dbMaster.getProject(null).get(i);
            assertEquals(t1,t2);
            assertEquals(p1,p2);
        }
    }
}

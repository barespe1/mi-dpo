/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.fit.dpo.mi_dpo_2013_skoulmir_module;

import cz.fit.dpo.mi_dpo_2013_skoulmir_dummy.ITask;
import cz.fit.dpo.mi_dpo_2013_skoulmir_dummy.Task;
import java.util.Date;
import static junit.framework.Assert.assertEquals;
import junit.framework.TestCase;

/**
 *
 * @author mini
 */
public class ConflictTest extends TestCase {
    
    public ConflictTest(String testName) {
        super(testName);
    }

    /**
     * Test of getMaster method, of class Conflict.
     */
    public void testGetMaster() {
        System.out.println("getMaster");
        ITask master = new Task(new Date(), new Date(), 25, "task", "desc", null, null);
        ITask local = new Task(new Date(), new Date(), 25, "task", "desc", null, null);
        Conflict instance = new Conflict(master,local);
        ITask expResult = master;
        ITask result = instance.getMaster();
        assertEquals(expResult, result);
    }

    /**
     * Test of getLocal method, of class Conflict.
     */
    public void testGetLocal() {
        System.out.println("getLocal");
        ITask master = new Task(new Date(), new Date(), 25, "task", "desc", null, null);
        ITask local = new Task(new Date(), new Date(), 25, "task", "desc", null, null);
        Conflict instance = new Conflict(master,local);
        ITask expResult = local;
        ITask result = instance.getLocal();
        assertEquals(expResult, result);
    }
}

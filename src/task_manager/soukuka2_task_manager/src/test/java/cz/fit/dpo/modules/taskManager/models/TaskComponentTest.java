/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.fit.dpo.modules.taskManager.models;


import cz.fit.dpo.mi_dpo_2013_skoulmir_dummy.ITask;
import cz.fit.dpo.mi_dpo_2013_skoulmir_dummy.Project;
import cz.fit.dpo.mi_dpo_2013_skoulmir_dummy.State;
import cz.fit.dpo.mi_dpo_2013_skoulmir_dummy.Task;
import cz.cvut.fit.spravaprojektu.usermanager.UserModel;

import cz.fit.dpo.modules.dbConnector.DBConnector;
import cz.fit.dpo.modules.taskManager.SimpleDbConnector;
import cz.fit.dpo.modules.taskManager.printers.CSVResultFactory;
import cz.fit.dpo.modules.taskManager.printers.CSVRowResult;
import cz.fit.dpo.modules.taskManager.printers.IOutputResult;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author karel-pc
 */
public class TaskComponentTest {
    
    protected DBConnector dbConnector;
    
    public TaskComponentTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        HashMap<Long, ITask> storage = new HashMap<>();    
        
        dbConnector = new SimpleDbConnector(storage);
    }
    
    @After
    public void tearDown() {
    }

    
    protected Task createTask() {
        Task task = new Task(new Date(), new Date(), 25, "task", "desc", null, null);
        task.setCosts(10);
        task.setName("Create DB connector");
        task.setOwner(createUser());
        task.setState(createState());
        task.setStart(new Date());
        task.setFinish(new Date());
        
        return task;
    }

    protected UserModel createUser() {
        UserModel user = new UserModel("");
        user.setFirstName("Karel");
        user.setLastName("Soukup");
        
        return user;
    }
    
    protected State createState() {
        State state = new State("");
        state.setDescription("V procesu");
        return state;
    }
    
    protected Project createProject() {
        Project project = new Project(new LinkedList<ITask>(), "", "", null, null);
        project.setName("Create DB connector");
        project.setOwner(createUser());
        project.setState(createState());
        Collection<ITask> projects = new ArrayList<>();
        project.setProjects(projects);

        
        return project;
    }
    
    /**
     * Test of getTaskIterator method, of class TaskComponent.
     */
    @Test
    public void testTaskIterator() {
        Task task = createTask();
        
        TaskManagerTask taskManagerTask = new TaskManagerTask(task, dbConnector);
        taskManagerTask.setOutputResultFactory(new CSVResultFactory());
        Iterator<IOutputResult> iterator = taskManagerTask.getTaskIterator();

        CSVRowResult csvRowResult = new CSVRowResult();
        csvRowResult.createFromTask(task);
       
        assertEquals(iterator.next().getResult(),csvRowResult.getResult());
        
        assertFalse(iterator.hasNext());
    }
    
    @Test
    public void testProjectIterator() {
        Project project = createProject();
        project.getProjects().add(createTask());
        project.getProjects().add(createTask());
        
        
        TaskManagerProject taskManagerProject = new TaskManagerProject(project, dbConnector);
        taskManagerProject.setOutputResultFactory(new CSVResultFactory());
        Iterator<IOutputResult> iterator = taskManagerProject.getTaskIterator();

        CSVRowResult csvRowResultProject = new CSVRowResult();
        csvRowResultProject.createFromProject(project);
        CSVRowResult csvRowResultTask = new CSVRowResult();
        csvRowResultTask.createFromTask(createTask());
        
        assertEquals(iterator.next().getResult(),csvRowResultProject.getResult());//project
        assertEquals(iterator.next().getResult(),csvRowResultTask.getResult());//task
        assertEquals(iterator.next().getResult(),csvRowResultTask.getResult());//task
        
        assertFalse(iterator.hasNext());
    }

    
}
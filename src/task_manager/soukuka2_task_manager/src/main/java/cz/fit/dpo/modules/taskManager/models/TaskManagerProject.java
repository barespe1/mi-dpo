/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.fit.dpo.modules.taskManager.models;

import cz.fit.dpo.mi_dpo_2013_skoulmir_dummy.ITask;
import cz.fit.dpo.mi_dpo_2013_skoulmir_dummy.Project;
import java.util.Iterator;
import cz.fit.dpo.modules.dbConnector.DBConnector;
import cz.fit.dpo.modules.taskManager.iterators.ProjectIterator;
import cz.fit.dpo.modules.taskManager.printers.IOutputResult;
import cz.fit.dpo.modules.taskManager.visitors.TaskFactoryVisitor;

/**
 *
 * @author karel-pc
 */
public class TaskManagerProject extends TaskComponent {

    protected Project project;
    
    protected DBConnector dbConnector;
    
    protected TaskFactoryVisitor taskFactoryVisitor;

    public TaskManagerProject(Project project, DBConnector dbConnector) {
        this.project = project;
        this.dbConnector = dbConnector;
        this.taskFactoryVisitor = new TaskFactoryVisitor(dbConnector);
    }

    @Override
    public Integer calculateCost() {
        Integer resultCost = 0;
        
        for(ITask task : project.getProjects()) {
            resultCost += taskFactoryVisitor.createTaskComponent(task).calculateCost();
        }
        
        return resultCost;
    }

    @Override
    public Iterator<IOutputResult> getTaskIterator() {
        if (outputResultFactory == null) {
            throw new RuntimeException("output result factory isnt set.");
        }
        
        return new ProjectIterator(project, outputResultFactory, taskFactoryVisitor);
    }

    @Override
    public void add() {
        dbConnector.addProject(project);
    }

    @Override
    public void edit() {
        dbConnector.editProject(project);
    }

    @Override
    public void remove() {
        dbConnector.removeProject(project);
    }


    
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.fit.dpo.modules.taskManager;

import cz.fit.dpo.modules.dbConnector.DBConnector;
import cz.fit.dpo.modules.taskManager.visitors.ITaskFactoryVisitor;
import cz.fit.dpo.modules.taskManager.visitors.TaskFactoryVisitor;

/**
 *
 * @author karel-pc
 */
public class TaskManagerFactory implements ITaskManagerFactory {

    @Override
    public ITaskManager createTaskManager(DBConnector dbConnector) {
        return new TaskManager(dbConnector,createTaskVisitor(dbConnector));
    }
    
    protected ITaskFactoryVisitor createTaskVisitor(DBConnector dbConnector) {
        return new TaskFactoryVisitor(dbConnector);
    }

    
}

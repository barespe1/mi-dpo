/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.fit.dpo.filters;

/**
 *
 * @author karel-pc
 */
public interface IProjectFilter {
    public Long getId();

    public void setId(Long id);
    
    public String getName();

    public void setName(String name);
}

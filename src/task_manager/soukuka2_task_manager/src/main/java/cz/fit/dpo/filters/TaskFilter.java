/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.fit.dpo.filters;

import cz.fit.dpo.models.State;
import cz.fit.dpo.models.User;

/**
 *
 * @author karel-pc
 */
public class TaskFilter implements ITaskFilter {
    protected Long id = null;
    protected String name = null;


    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }
    
    @Override
    public String getName() {
        return name;
    }

    @Override
    public void setName(String name) {
        this.name = name;
    }
}

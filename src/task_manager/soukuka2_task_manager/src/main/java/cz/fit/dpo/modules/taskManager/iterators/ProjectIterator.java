/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.fit.dpo.modules.taskManager.iterators;


import cz.fit.dpo.mi_dpo_2013_skoulmir_dummy.ITask;
import cz.fit.dpo.mi_dpo_2013_skoulmir_dummy.Project;
import java.util.Iterator;
import cz.fit.dpo.modules.taskManager.models.TaskComponent;
import cz.fit.dpo.modules.taskManager.printers.IOutputResult;
import cz.fit.dpo.modules.taskManager.printers.IOutputResultFactory;
import cz.fit.dpo.modules.taskManager.visitors.ITaskFactoryVisitor;

/**
 *
 * @author karel-pc
 */
public class ProjectIterator implements Iterator<IOutputResult> {

    protected Project project;
    protected Iterator<ITask> subProjectsIterator;
    protected boolean isReturnedProject = false;
    protected IOutputResultFactory outputResultFactory;
    protected ITaskFactoryVisitor taskFactoryVisitor;
    protected Iterator<IOutputResult> childInProcess = null;

    public ProjectIterator(Project project, IOutputResultFactory outputResultFactory, ITaskFactoryVisitor taskFactoryVisitor) {
        this.project = project;
        this.subProjectsIterator = project.getProjects().iterator();
        this.outputResultFactory = outputResultFactory;
        this.taskFactoryVisitor = taskFactoryVisitor;
    }

    @Override
    public boolean hasNext() {
        if (isReturnedProject == false) {
            return true;
        }

        if (subProjectsIterator.hasNext()) {
            return true;
        }

        return false;
    }

    @Override
    public IOutputResult next() {
        while (hasNext()) {
            if (isReturnedProject == false) {
                isReturnedProject = true;
                IOutputResult outputResult = outputResultFactory.createOutputResult();
                outputResult.createFromProject(project);
                return outputResult;
            }

            if (childInProcess == null) {
                TaskComponent taskComponent = taskFactoryVisitor.createTaskComponent(subProjectsIterator.next());
                taskComponent.setOutputResultFactory(outputResultFactory);
                childInProcess = taskComponent.getTaskIterator();                
            }

            if (childInProcess.hasNext()) {
                IOutputResult result = childInProcess.next();
                childInProcess = null;
                return result;
            }
            
        }
        return null;
    }

    @Override
    public void remove() {
        throw new UnsupportedOperationException();
    }
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.fit.dpo.modules.dbConnector;

/**
 *
 * @author karel-pc
 */
public interface DBConnectorFactory {
    public DBConnector createDBConnector(String path);
}

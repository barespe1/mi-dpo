/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.fit.dpo.modules.taskManager.iterators;

import cz.fit.dpo.mi_dpo_2013_skoulmir_dummy.Task;
import java.util.Iterator;
import cz.fit.dpo.modules.taskManager.printers.IOutputResult;
import cz.fit.dpo.modules.taskManager.printers.IOutputResultFactory;

/**
 *
 * @author karel-pc
 */
public class TaskIterator implements Iterator<IOutputResult> {

    boolean hasNext = true;
    
    Task task;
    IOutputResultFactory outputResultFactory;

    public TaskIterator(Task task, IOutputResultFactory outputResultFactory) {
        this.task = task;
        this.outputResultFactory = outputResultFactory;
    }
       
    @Override
    public boolean hasNext() {
        return hasNext;
    }

    @Override
    public IOutputResult next() {
        if (hasNext == false){
            return null;
        }
        hasNext = false;
        
        IOutputResult outputResult = outputResultFactory.createOutputResult();
        outputResult.createFromTask(task);
        return outputResult;
    }

    @Override
    public void remove() {
        throw new UnsupportedOperationException();
    }


    
}

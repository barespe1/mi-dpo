/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.fit.dpo.modules.taskManager;

import cz.fit.dpo.filters.IProjectFilter;
import cz.fit.dpo.filters.ITaskFilter;
import cz.fit.dpo.filters.ProjectFilter;
import cz.fit.dpo.filters.TaskFilter;
import cz.fit.dpo.mi_dpo_2013_skoulmir_dummy.ITask;
import cz.fit.dpo.mi_dpo_2013_skoulmir_dummy.Project;
import cz.fit.dpo.mi_dpo_2013_skoulmir_dummy.Task;
import cz.fit.dpo.mi_dpo_2013_skoulmir_module.Sync;
import java.io.File;
import java.util.List;
import cz.fit.dpo.modules.dbConnector.DBConnector;
import cz.fit.dpo.modules.taskManager.models.TaskComponent;
import cz.fit.dpo.modules.taskManager.printers.CSVResultFactory;
import cz.fit.dpo.modules.taskManager.printers.IOutputResult;
import cz.fit.dpo.modules.taskManager.printers.IOutputResultFactory;
import cz.fit.dpo.modules.taskManager.visitors.ITaskFactoryVisitor;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Iterator;

/**
 *
 * @author karel-pc
 */
public class TaskManager implements ITaskManager {

    ITaskFactoryVisitor taskVisitor;
    DBConnector dbConnector;

    public TaskManager(DBConnector dbConnector, ITaskFactoryVisitor taskVisitor) {
        this.dbConnector = dbConnector;
        this.taskVisitor = taskVisitor;
    }

    @Override
    public void add(ITask task) {
        taskVisitor.createTaskComponent(task).add();
    }

    @Override
    public void edit(ITask task) {
        taskVisitor.createTaskComponent(task).edit();
    }

    @Override
    public void remove(ITask task) {
        taskVisitor.createTaskComponent(task).remove();
    }

    @Override
    public List<Project> getProjects(IProjectFilter projectFilter) {
        return dbConnector.getProjects(projectFilter);
    }

    @Override
    public List<Task> getTasks(ITaskFilter taskFilter) {
        return dbConnector.getTasks(taskFilter);
    }

    @Override
    public File exportToCSV(ITask task) {
        try {
            File tmpFile = File.createTempFile("csv-temp-file", ".csv");
            FileWriter fstream = new FileWriter(tmpFile);
            BufferedWriter out = new BufferedWriter(fstream);

            TaskComponent taskComponent = taskVisitor.createTaskComponent(task);

            IOutputResultFactory outputResultFactory = new CSVResultFactory();
            taskComponent.setOutputResultFactory(outputResultFactory);

            Iterator<IOutputResult> csvRowIterator = taskComponent.getTaskIterator();

            while (csvRowIterator.hasNext()) {
                out.write(csvRowIterator.next().getResult());
            }

            out.close();
            fstream.close();

            return tmpFile;

        } catch (IOException ex) {
            throw new RuntimeException("Create temporary file errror.");
        }
    }

    @Override
    public Task getTaskById(Long id) {
        TaskFilter taskFilter = new TaskFilter();
        taskFilter.setId(id);

        List<Task> tasks = dbConnector.getTasks(taskFilter);

        if (tasks.isEmpty()) {
            return null;
        }

        return tasks.get(0);
    }

    @Override
    public Project getProjectById(Long id) {
        ProjectFilter projectFilter = new ProjectFilter();
        projectFilter.setId(id);

        List<Project> projects = dbConnector.getProjects(projectFilter);

        if (projects.isEmpty()) {
            return null;
        }

        return projects.get(0);
    }

    @Override
    public Integer getCost(ITask task) {
        return taskVisitor.createTaskComponent(task).calculateCost();
    }
}
